package composite;

public class Ram implements Laiteosa {
	
	private int hinta = 160;
	
	public int getHinta() {
		return hinta;
	}

	@Override
	public String getKomponenttiNimi() {
		return "Ram";
	}

}
