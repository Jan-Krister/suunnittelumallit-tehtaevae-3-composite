package composite;

public class Kovalevy implements Laiteosa {
	
	private int hinta = 145;

	@Override
	public int getHinta() {
		return hinta;
	}

	@Override
	public String getKomponenttiNimi() {
		return "Kovalevy";
	}
	

}
