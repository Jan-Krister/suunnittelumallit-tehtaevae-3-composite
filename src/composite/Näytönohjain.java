package composite;

public class Näytönohjain implements Laiteosa {
	
	private int hinta = 500;

	@Override
	public int getHinta() {
		return hinta;
	}

	@Override
	public String getKomponenttiNimi() {
		return "Näytönohjain";
	} 

}
