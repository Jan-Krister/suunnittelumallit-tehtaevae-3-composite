package composite;

import java.util.ArrayList;

public class Kotelo implements Laiteosa {

	KomponenttiTehdas tehdas;
	private int hinta = 125;
	private ArrayList<Laiteosa> kLista = new ArrayList<Laiteosa>();

	public Kotelo(KomponenttiTehdas tehdas) {
		this.tehdas = tehdas;
	}

	public int getHinta() {
		int yhteensä = 0;
		for (int i = 0; i < kLista.size(); i++) {
			yhteensä += kLista.get(i).getHinta();
		}
		return hinta + yhteensä;
	}

	@Override
	public Laiteosa lisääKomponentti(int valinta) throws Exception {
		if (valinta != 2 && valinta != 7) {
			throw new Exception("Valittua komponenttia ei voi lisätä tähän komponenttiin");
		} else {
			kLista.add(tehdas.haeKomponentti(valinta));
			return kLista.get(kLista.size() - 1);
		}
	}

	public String getKomponenttiNimi() {
		return "kotelo";
	}

}