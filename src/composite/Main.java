package composite;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		KomponenttiTehdas tehdas = new KomponenttiTehdas();
		ArrayList<Laiteosa> laiteosa = new ArrayList<>();
		@SuppressWarnings("resource")
		Scanner skanneri = new Scanner(System.in);
		boolean käynnissä = true;
		int index = 0;

		while (käynnissä) {
			System.out.println("Valitse vaihtoehto:");
			System.out.println("1 Listaa komponentit");
			System.out.println("2 lisää komponentti (1-7)");
			System.out.println("3 ilmoita käsittelyssä oleva komponentti");
			System.out.println("4 listaa kokoonpanon hinta");
			System.out.println("5 Siirry komponenttilistassa ylöspäin");
			System.out.println("6 Siirry komponenttilistassa alaspäin");
			int valinta = skanneri.nextInt();
			try {
				switch (valinta) {
				case 1:
					System.out.println(
							"1: Kotelo, 2: Emolevy, 3: RAM, 4: Prosessori, 5: Kovalevy, 6: Näytonohjain, 7: Virtalähde");
					break;
				case 2:
					System.out.println(
							"Valitseppas listasta: 1: Kotelo, 2: Emolevy, 3: RAM, 4: Prosessori, 5: Kovalevy, 6: Näytonohjain, 7: Virtalähde");
					int valinta2 = skanneri.nextInt();
					if (laiteosa.size() == 0) {
						laiteosa.add(tehdas.haeKomponentti(valinta2));
					} else {
						laiteosa.add(laiteosa.get(index).lisääKomponentti(valinta2));
					}
					break;
				case 3:
					System.out.println(laiteosa.get(index).getKomponenttiNimi());
					break;
				case 4:
					System.out.println(laiteosa.get(index).getHinta());
					break;
				case 5:
					if (index > 0) {
						index--;
					} else {
						System.out.println("Indeksi on huipulla");
					}
					break;
				case 6:
					if (laiteosa.size() > index + 1) {
						index++;
					} else {
						System.out.println("Indeksi on pohjalla");
					}
					break;
				default:
					throw new Exception("Kokeile numeroa 1-6");
				}
			} catch (Exception e) {
				System.out.println(e);
			}
			System.out.println();

		}
	}
}
