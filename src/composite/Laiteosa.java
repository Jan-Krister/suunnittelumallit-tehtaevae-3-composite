package composite;

public interface Laiteosa {

	int getHinta();

	String getKomponenttiNimi();
	
	default public Laiteosa lisääKomponentti(int valinta) throws Exception {
		throw new Exception("Valittua komponenttia ei voi lisätä tähän komponenttiin");
	}
}
