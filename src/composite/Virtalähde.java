package composite;

public class Virtalähde implements Laiteosa {
	
	private int hinta = 135;

	@Override
	public int getHinta() {
		return hinta;
	}

	@Override
	public String getKomponenttiNimi() {
		return "Virtalähde";
	}
	

}
