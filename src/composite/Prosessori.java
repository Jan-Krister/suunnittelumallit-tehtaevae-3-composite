package composite;

public class Prosessori implements Laiteosa {

	private int hinta = 300;

	public int getHinta() {
		return hinta;
	}

	public String getKomponenttiNimi() {
		return "Prosessori";
	}
}
