package composite;

import java.util.ArrayList;

public class Emolevy implements Laiteosa {

	KomponenttiTehdas tehdas;
	@SuppressWarnings("unchecked")
	private ArrayList<Laiteosa> kLista = new ArrayList();
	private int hinta = 149;

	public Emolevy(KomponenttiTehdas tehdas) {
		this.tehdas = tehdas;
	}

	@Override
	public Laiteosa lisääKomponentti(int valinta) throws Exception {
		if (valinta == 2 || valinta == 7 || valinta == 1) {
			throw new Exception("Valittua komponenttia ei voi lisätä tähän komponenttiin");
		} else {
			kLista.add(tehdas.haeKomponentti(valinta));
			return kLista.get(kLista.size() - 1);
		}
	}

	public int getHinta() {
		int yhteensä = 0;
		for (int i = 0; i < kLista.size(); i++) {
			yhteensä += kLista.get(i).getHinta();
		}
		return hinta + yhteensä;
	}

	public String getKomponenttiNimi() {
		return "Emolevy";
	}
}
