package composite;

public class KomponenttiTehdas {
	
	public Laiteosa haeKomponentti(int valinta) throws Exception {
		switch (valinta) {
        case 1: return new Kotelo(this);
        case 2: return new Emolevy(this);
        case 3: return new Ram();
        case 4: return new Prosessori();
        case 5: return new Kovalevy();
        case 6: return new Näytönohjain();
        case 7: return new Virtalähde();
        default: throw new Exception("Kokeile numeroa 1-7");
		}
	}

}
